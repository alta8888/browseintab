# BrowseInTab

Open and browse any link in a Thunderbird content tab. Links are followed within the tab and back/forward navigation is supported. Also with enhanced Zoom customization.