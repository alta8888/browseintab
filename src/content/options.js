"use strict";

/* globals browser */

var BrowseInTabOptions = {
  e(element) {
    return document.getElementById(element);
  },

  maxContentTabsDefault: 10,

  onLoad() {
    // console.debug("BrowseInTabOptions.onLoad:");
    this.initializeElements();

    /* eslint-disable */
    this.maxContentTabs = this.e("maxContentTabs");
    this.tabsLoadInBackground = this.e("tabsLoadInBackground");
    this.linkClickLoadsInTab = this.e("linkClickLoadsInTab");
    this.linkClickOptions = this.e("linkClickOptions");
    this.linkClickLoadsInBackgroundTab = this.e("linkClickLoadsInBackgroundTab");
    this.forceLinkClickLoadsInCurrentTab = this.e("forceLinkClickLoadsInCurrentTab");
    this.useFirefoxCompatUserAgent = this.e("useFirefoxCompatUserAgent");

    this.restoreOptions();
    this.e("container").removeAttribute("hidden");

    this.maxContentTabs.addEventListener(
      "change",
      this.saveOptions.bind(this)
    );
    /* eslint-enable */
    this.tabsLoadInBackground.addEventListener(
      "change",
      this.saveOptions.bind(this)
    );
    this.linkClickLoadsInTab.addEventListener(
      "change",
      this.saveOptions.bind(this)
    );
    this.linkClickLoadsInBackgroundTab.addEventListener(
      "change",
      this.saveOptions.bind(this)
    );
    this.forceLinkClickLoadsInCurrentTab.addEventListener(
      "change",
      this.saveOptions.bind(this)
    );
    this.useFirefoxCompatUserAgent.addEventListener(
      "change",
      this.saveOptions.bind(this)
    );
  },

  initializeElements() {
    // Adding browser_style to options_ui or class="browser-style" has 0 effect.
    let i18nString = browser.i18n.getMessage;
    let html = `
      <label for="maxContentTabs">${i18nString("maxContentTabs")}</label>
      <input id="maxContentTabs"
             type="number"
             min="0"
             style="width: 3.1em; text-align: end;"></input>
      <div style="margin-bottom: 10px;"></div>

      <input id="tabsLoadInBackground"
             type="checkbox"></input>
      <label for="tabsLoadInBackground">
        ${i18nString("tabsLoadInBackground")}</label>
      <div></div>
      <label style="margin-inline-start: 30px; font-size: smaller;">
        ${i18nString("tabsLoadInBackgroundExtra")}</label>
      <div style="margin-bottom: 10px;"></div>

      <input id="linkClickLoadsInTab"
             type="checkbox"></input>
      <label for="linkClickLoadsInTab">
        ${i18nString("linkClickLoadsInTab")}</label>
      <div></div>
      <label style="margin-inline-start: 30px; font-size: smaller;">
        ${i18nString("linkClickLoadsInTabExtra")}</label>
      <div></div>

      <fieldset id="linkClickOptions"
                style="margin: 6px 0 0 30px; padding: 0; border: none;">
        <input id="linkClickLoadsInBackgroundTab"
               type="checkbox"></input>
        <label for="linkClickLoadsInBackgroundTab">
          ${i18nString("linkClickLoadsInBackgroundTab")}</label>
        <div></div>
        <label style="margin-inline-start: 30px; font-size: smaller;">
          ${i18nString("linkClickLoadsInBackgroundTabExtra")}</label>
      </fieldset>
      <div style="margin-bottom: 10px;"></div>

      <input id="forceLinkClickLoadsInCurrentTab"
             type="checkbox"></input>
      <label for="forceLinkClickLoadsInCurrentTab">
        ${i18nString("forceLinkClickLoadsInCurrentTab")}</label>
      <div></div>
      <label style="margin-inline-start: 30px; font-size: smaller;">
        ${i18nString("forceLinkClickLoadsInCurrentTabExtra")}</label>
      <div style="margin-bottom: 10px;"></div>

      <input id="useFirefoxCompatUserAgent"
             type="checkbox"></input>
      <label for="useFirefoxCompatUserAgent">
        ${i18nString("useFirefoxCompatUserAgent")}</label>
      <div></div>
      <label style="margin-inline-start: 30px; font-size: smaller;">
        ${i18nString("useFirefoxCompatUserAgentExtra")}</label>

      <br/><br/>
      <div>
        <label style="width: -moz-available; font-size: smaller;">${i18nString(
          "systemContentTabOrLinkNote"
        )}</label>
      </div>
    `;

    let range = document.createRange();
    let parent = document.getElementById("container");
    range.selectNode(parent);
    // eslint-disable-next-line no-unsanitized/method
    let documentFragment = range.createContextualFragment(html);
    parent.appendChild(documentFragment);
  },

  updateElements(checked) {
    if (checked) {
      this.linkClickOptions.disabled = false;
      this.linkClickOptions.style.color = "";
    } else {
      this.linkClickOptions.disabled = true;
      this.linkClickOptions.style.color = "GrayText";
    }
  },

  saveOptions() {
    // Just set them all.
    /* eslint-disable */
    let storageLocalData = {
      maxContentTabs: Number(this.maxContentTabs.value),
      tabsLoadInBackground: this.tabsLoadInBackground.checked,
      linkClickLoadsInTab: this.linkClickLoadsInTab.checked,
      linkClickLoadsInBackgroundTab: this.linkClickLoadsInBackgroundTab.checked,
      forceLinkClickLoadsInCurrentTab: this.forceLinkClickLoadsInCurrentTab.checked,
      useFirefoxCompatUserAgent: this.useFirefoxCompatUserAgent.checked,
    };
    /* eslint-enable */

    this.updateElements(this.linkClickLoadsInTab.checked);

    browser.storage.local.set(storageLocalData);
    browser.browseintab.notifyStorageLocal(storageLocalData, false);
  },

  restoreOptions() {
    let setCurrentChoice = result => {
      this.maxContentTabs.value =
        "maxContentTabs" in result
          ? result.maxContentTabs
          : this.maxContentTabsDefault;
      this.tabsLoadInBackground.checked =
        "tabsLoadInBackground" in result ? result.tabsLoadInBackground : true;
      this.linkClickLoadsInTab.checked =
        "linkClickLoadsInTab" in result ? result.linkClickLoadsInTab : false;
      this.linkClickLoadsInBackgroundTab.checked =
        "linkClickLoadsInBackgroundTab" in result
          ? result.linkClickLoadsInBackgroundTab
          : false;
      this.forceLinkClickLoadsInCurrentTab.checked =
        "forceLinkClickLoadsInCurrentTab" in result
          ? result.forceLinkClickLoadsInCurrentTab
          : true;
      this.useFirefoxCompatUserAgent.checked =
        "useFirefoxCompatUserAgent" in result
          ? result.useFirefoxCompatUserAgent
          : false;
      // console.debug("BrowseInTabOptions.restoreOptions: result - ");
      // console.dir(result);

      this.updateElements(this.linkClickLoadsInTab.checked);
    };

    let onError = error => {
      console.error(`BrowseInTabOptions.restoreOptions: ${error}`);
    };

    let getting = browser.storage.local.get([
      "maxContentTabs",
      "tabsLoadInBackground",
      "linkClickLoadsInTab",
      "linkClickLoadsInBackgroundTab",
      "forceLinkClickLoadsInCurrentTab",
      "useFirefoxCompatUserAgent",
    ]);
    getting.then(setCurrentChoice, onError);
  },
};

document.addEventListener(
  "DOMContentLoaded",
  () => {
    BrowseInTabOptions.onLoad();
  },
  { once: true }
);
