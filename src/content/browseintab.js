"use strict";

ChromeUtils.import("resource://gre/modules/Services.jsm");

/* globals AppConstants, gContextMenu, hRefForClickEvent, MozXULElement,
 *         nsContextMenu, openLinkExternally, openTab, Services, specialTabs,
 *         urlSecurityCheck */

if (typeof BrowseInTab == "object") {
  BrowseInTab.onUnload();
}

var BrowseInTab = {
  e(element) {
    return document.getElementById(element);
  },

  get addonId() {
    return "browseintab@mozdev.org";
  },

  get obsTopicLocaleMessages() {
    return `extension:${this.addonId}:locale-messages`;
  },

  get obsTopicStorageLocal() {
    return `extension:${this.addonId}:storage-local`;
  },

  get obsNotificationReadyTopic() {
    return `extension:${this.addonId}:ready`;
  },

  /*
   * Preferences.
   */
  get storageLocalMap() {
    return this._storageLocalMap || new Map();
  },

  set storageLocalMap(val) {
    this._storageLocalMap = val;
  },

  getStorageLocal(key) {
    return this.storageLocalMap.get(key);
  },

  get maxContentTabsPref() {
    let prefKey = "maxContentTabs";
    let prefValue = this.getStorageLocal(prefKey);
    let defaultValue = specialTabs.contentTabType.modes.contentTab.maxTabs;
    if (prefValue == undefined) {
      return defaultValue;
    }
    specialTabs.contentTabType.modes.contentTab.maxTabs = prefValue;
    return prefValue;
  },

  get loadInBackgroundPref() {
    let prefKey = "tabsLoadInBackground";
    let prefValue = this.getStorageLocal(prefKey);
    let defaultValue = Services.prefs.getBoolPref("mail.tabs.loadInBackground");
    if (prefValue == undefined) {
      return defaultValue;
    }
    Services.prefs.setBoolPref("mail.tabs.loadInBackground", prefValue);
    return prefValue;
  },

  get linkClickLoadsInTabPref() {
    let prefKey = "linkClickLoadsInTab";
    let prefValue = this.getStorageLocal(prefKey);
    let defaultValue = false;
    if (prefValue == undefined) {
      return defaultValue;
    }
    return prefValue;
  },

  get linkClickLoadsInBackgroundTabPref() {
    let prefKey = "linkClickLoadsInBackgroundTab";
    let prefValue = this.getStorageLocal(prefKey);
    let defaultValue = false;
    if (prefValue == undefined) {
      return defaultValue;
    }
    return prefValue;
  },

  get forceLinkClickLoadsInCurrentTabPref() {
    let prefKey = "forceLinkClickLoadsInCurrentTab";
    let prefValue = this.getStorageLocal(prefKey);
    let defaultValue = true;
    if (prefValue == undefined) {
      return defaultValue;
    }
    return prefValue;
  },

  get useFirefoxCompatUserAgentPref() {
    let prefKey = "useFirefoxCompatUserAgent";
    let prefValue = this.getStorageLocal(prefKey);
    let defaultValue = false;
    if (prefValue == undefined) {
      return defaultValue;
    }
    return prefValue;
  },

  /*
   * Strings.
   */
  get localeMessagesMap() {
    return this._localeMessagesMap || new Map();
  },

  set localeMessagesMap(val) {
    this._localeMessagesMap = val;
  },

  getLocaleMessage(key) {
    return this.localeMessagesMap.get(key.toLowerCase()) || "";
  },

  onLoad() {
    Services.obs.addObserver(this.Observer, this.obsTopicLocaleMessages);
    Services.obs.addObserver(this.Observer, this.obsTopicStorageLocal);
    Services.obs.notifyObservers(null, this.obsNotificationReadyTopic);

    let tabmail = this.e("tabmail");
    if (tabmail) {
      tabmail.registerTabMonitor(this.TabMonitor);
    }

    this.e("messagepane").addEventListener(
      "click",
      event => this.contentAreaClick(event),
      { capture: true }
    );

    AddonManager.addAddonListener(this.AddonListener);

    // console.debug("onLoad: observers added ");

    /* Override Tb native functions *******************************************/
    if ("openLinkExternally" in window) {
      this._openLinkExternally = openLinkExternally;
      // eslint-disable-next-line no-global-assign
      openLinkExternally = (url, event) => {
        this.openLinkExternally(url, event);
      };
    }

    this.InitializeTabs();
  },

  onUnload() {
    // console.debug("onUnload: --> BrowseInTab ");
    this.RemoveObservers();
    this.RestoreOverrideFunctions();
    this.RestoreOverlayElements();
    this.RestoreTabs();

    // RemoveStyles()
    let styleSheet = document.querySelector(`style[title="${this.addonId}"]`);
    if (styleSheet) {
      styleSheet.remove();
    }

    // console.debug("onUnload: --> BrowseInTab DONE");
  },

  InitializeOverlayElements() {
    // console.debug("InitializeOverlayElements: ");
    // Keys.
    let modifier = AppConstants.platform == "macosx" ? "accel" : "alt";
    this.e("mailKeys").appendChild(
      MozXULElement.parseXULToFragment(`
      <key id="goBackKb_BiT" extension="${this.addonId}"
           keycode="VK_LEFT"
           modifiers="${modifier}"
           oncommand="BrowseInTab.BrowserController.doCommand('Browser:Back_BiT')"/>
      <key id="goForwardKb_BiT" extension="${this.addonId}"
           keycode="VK_RIGHT"
           modifiers="${modifier}"
           oncommand="BrowseInTab.BrowserController.doCommand('Browser:Forward_BiT')"/>
      `)
    );

    // Context menu. Menuitems for links and back/forward.
    /* eslint-disable */
    this.e("mailContext").insertBefore(
      MozXULElement.parseXULToFragment(`
      <menuitem id="mailContext-openLinkInContentTab" extension="${this.addonId}"
                label="${this.getLocaleMessage("openLinkInContentTabLabel")}"
                accesskey="${this.getLocaleMessage("openLinkInContentTabAccesskey")}"
                oncommand="BrowseInTab.openLinkInContentTab(event, 'tab');"/>
      `),
      this.e("mailContext-sep-open-browser")
    );
    this.e("mailContext").insertBefore(
      MozXULElement.parseXULToFragment(`
      <menuitem id="context-back_BiT" extension="${this.addonId}"
                class="menuitem-iconic"
                label="&goBackCmd.label;"
                command="Browser:Back_BiT"/>
      <menuitem id="context-forward_BiT" extension="${this.addonId}"
                class="menuitem-iconic"
                label="&goForwardCmd.label;"
                command="Browser:Forward_BiT"/>
      `, ["chrome://messenger/locale/messenger.dtd"]
      ),
      this.e("mailContext-reload")
    );
    /* eslint-enable */

    this.e("mailContext").addEventListener(
      "popupshowing",
      this.onMailContextPopupShowing
    );

    // Context menu for copyUrlPopup. Menuitems for <mail-urlfield> link.
    /* eslint-disable */
    let copyUrlPopup = this.e("copyUrlPopup");
    copyUrlPopup.insertBefore(
      MozXULElement.parseXULToFragment(`
      <menuitem id="copyUrlPopup-openLinkInBrowser" extension="${this.addonId}"
                label="&openLinkInBrowser.label;"
                accesskey="&openLinkInBrowser.accesskey;"
                oncommand="BrowseInTab.openCopyUrlPopupLink(event, 'browser');"/>
      <menuitem id="copyUrlPopup-openLinkInContentTab" extension="${this.addonId}"
                label="${this.getLocaleMessage("openLinkInContentTabLabel")}"
                accesskey="${this.getLocaleMessage("openLinkInContentTabAccesskey")}"
                oncommand="BrowseInTab.openCopyUrlPopupLink(event, 'tab');"/>
      `, ["chrome://messenger/locale/messenger.dtd"]
      ),
      copyUrlPopup.lastElementChild
    );
    /* eslint-enable */
  },

  RestoreOverlayElements() {
    // console.debug("RestoreOverlayElements: ");
    let nodes = document.querySelectorAll(`[extension="${this.addonId}"]`);
    for (let node of nodes) {
      node.remove();
    }
  },

  RestoreOverrideFunctions() {
    // eslint-disable-next-line no-global-assign
    openLinkExternally = BrowseInTab._openLinkExternally;
  },

  RemoveObservers() {
    for (let observer of Services.obs.enumerateObservers(
      this.obsTopicStorageLocal
    )) {
      Services.obs.removeObserver(observer, this.obsTopicStorageLocal);
    }

    let tabmail = this.e("tabmail");
    if (tabmail) {
      tabmail.unregisterTabMonitor(this.TabMonitor);
    }

    this.e("mailContext").removeEventListener(
      "popupshowing",
      this.onMailContextPopupShowing
    );

    AddonManager.removeAddonListener(this.AddonListener);
  },

  InitializeTabs() {
    let tabmail = document.getElementById("tabmail");
    if (!tabmail) {
      return;
    }
    let tabsInfo = tabmail.tabInfo;
    for (let tabInfo of tabsInfo) {
      this.InitializeTab(tabInfo);
    }
  },

  /**
   * Ensure our siteClickHandler is added to the tab.
   *
   * @param {TabInfo} tabInfo - the tabInfo for the tab.
   */
  InitializeTab(tabInfo) {
    if (
      tabInfo.mode.type != "contentTab" ||
      tabInfo.clickHandler.startsWith("BrowseInTab.")
    ) {
      return;
    }
    tabInfo.clickHandler = "BrowseInTab.siteClickHandler(event)";
    tabInfo.browser.setAttribute("onclick", tabInfo.clickHandler);
  },

  RestoreTabs() {
    let tabsInfo = document.getElementById("tabmail").tabInfo;
    for (let tabInfo of tabsInfo) {
      if (
        tabInfo.mode.type != "contentTab" ||
        !tabInfo.clickHandler.startsWith("BrowseInTab.")
      ) {
        continue;
      }
      tabInfo.clickHandler = "specialTabs.defaultClickHandler(event);";
      tabInfo.browser.setAttribute("onclick", tabInfo.clickHandler);
    }
  },

  Observer: {
    observe(aSubject, aTopic, aData) {
      if (aTopic == BrowseInTab.obsTopicLocaleMessages) {
        // console.debug("Observer: " + aTopic + ", data - " + aData);
        BrowseInTab.localeMessagesMap = new Map(JSON.parse(aData));
        // Initialize now that we have the locale map, only on startup, once.
        BrowseInTab.InitializeOverlayElements();
        Services.obs.removeObserver(
          BrowseInTab.Observer,
          BrowseInTab.obsTopicLocaleMessages
        );
      } else if (aTopic == BrowseInTab.obsTopicStorageLocal) {
        // console.debug("Observer: " + aTopic + ", data - " + aData);
        BrowseInTab.storageLocalMap = new Map(JSON.parse(aData));
        // Initialize these system 'prefs'.
        BrowseInTab.maxContentTabsPref;
        BrowseInTab.loadInBackgroundPref;
      }
    },
  },

  TabMonitor: {
    monitorName: "BrowseInTab",
    onTabTitleChanged() {},
    onTabSwitched(tab, oldTab) {},
    onTabOpened(tab, firstTab, oldTab) {
      // console.debug("onTabOpened: title - " + tab.title);
      BrowseInTab.InitializeTab(tab);
    },
  },

  BrowserController: {
    supportsCommand(command) {
      // console.debug("BrowserController.supportsCommand: command - " + command);
      switch (command) {
        case "Browser:Back_BiT":
        case "Browser:Forward_BiT":
          return true;
        default:
          return false;
      }
    },
    isCommandEnabled(command) {
      // console.debug("BrowserController.isCommandEnabled: command - " + command);
      let tabmail = document.getElementById("tabmail");
      let browser = tabmail ? tabmail.getBrowserForSelectedTab() : null;
      if (!browser || !browser.sessionHistory) {
        return false;
      }
      switch (command) {
        case "Browser:Forward_BiT":
          return browser.canGoForward;
        case "Browser:Back_BiT":
          return browser.canGoBack;
        default:
          return false;
      }
    },
    doCommand(command) {
      if (!this.isCommandEnabled(command)) {
        return;
      }
      // console.debug("BrowserController.doCommand: command - " + command);
      let tabmail = document.getElementById("tabmail");
      let browser = tabmail ? tabmail.getBrowserForSelectedTab() : null;
      if (!browser || !browser.sessionHistory) {
        return;
      }
      switch (command) {
        case "Browser:Forward_BiT":
          browser.goForward();
          break;
        case "Browser:Back_BiT":
          browser.goBack();
          break;
      }
    },
    onEvent(event) {},
  },

  /*
   * Listener for addon status changes.
   */
  AddonListener: {
    resetSession(addon, who) {
      if (addon.id != BrowseInTab.addonId) {
        return;
      }
      // console.debug("AddonListener.resetSession: who - " + who);
      BrowseInTab.onUnload();
      console.info(BrowseInTab.getLocaleMessage("extensionBye"));
    },
    onUninstalling(addon) {
      this.resetSession(addon, "onUninstalling");
    },
    onInstalling(addon) {
      this.resetSession(addon, "onInstalling");
    },
    onDisabling(addon) {
      this.resetSession(addon, "onDisabling");
    },
    // The listener is removed so these aren't run; they aren't needed as the
    // addon is installed by the addon system and runs our backgound.js loader.
    onEnabling(addon) {},
    onOperationCancelled(addon) {},
  },

  /**
   * Set up the menuitems. The default onpopupshowing function will create
   * gContextMenu global.
   */
  onMailContextPopupShowing(event) {
    if (!gContextMenu) {
      return;
    }
    // Only show mailContext-openLinkInContentTab if we're on a link and
    // it isn't a mailto link.
    gContextMenu.showItem(
      "mailContext-openLinkInContentTab",
      gContextMenu.onLink &&
        !gContextMenu.onMailtoLink &&
        gContextMenu.linkProtocol != "about" &&
        gContextMenu.linkProtocol != "chrome"
    );

    // Work out if we are a context menu on a special item e.g. an image, link.
    let notOnSpecialItem = !(
      gContextMenu.inMessageArea ||
      gContextMenu.isContentSelected ||
      gContextMenu.onCanvas ||
      gContextMenu.onLink ||
      gContextMenu.onImage ||
      gContextMenu.onPlayableMedia ||
      gContextMenu.onTextInput
    );

    // Ensure these commands are updated with their current status.
    if (notOnSpecialItem) {
      gContextMenu.enableItem(
        "context-back_BiT",
        BrowseInTab.BrowserController.isCommandEnabled("Browser:Back_BiT")
      );
      gContextMenu.enableItem(
        "context-forward_BiT",
        BrowseInTab.BrowserController.isCommandEnabled("Browser:Forward_BiT")
      );
    }
    // These only needs showing if we're not on something special.
    gContextMenu.showItem("context-back_BiT", notOnSpecialItem);
    gContextMenu.showItem("context-forward_BiT", notOnSpecialItem);
  },

  /*
   * Get the linkNode since openLinkExternally doesn't pass an event currently.
   */
  contentAreaClick(event) {
    let [href] = hRefForClickEvent(event);
    if (!href) {
      return;
    }

    this.savedContentAreaClickEvent = event;

    if (["mailto:", "nntp:", "news:"].includes(new URL(href).protocol)) {
      let linkNode = event.target;
      // console.dir(linkNode.attributes);
      if (linkNode.hasAttribute("target")) {
        linkNode.setAttribute("_target", linkNode.getAttribute("target"));
        linkNode.removeAttribute("target");
      }
      // console.dir(linkNode.attributes);
    }
    // console.dir(this.savedContentAreaClickEvent);
  },

  /**
   * Open a link in Tb, via copyUrlPopup contextmenu for <mail-urlfield>.
   *
   * @param {Event} event   - An event.
   * @param {String} where  - 'tab' or 'browser'.
   */
  openCopyUrlPopupLink(event, where) {
    let linkNode = document.popupNode;
    let linkUrl = linkNode.textContent;
    // console.debug("openLinkInContentTab: START url - " + linkUrl);
    if (!linkUrl || event.detail == 2 || event.button == 2) {
      return;
    }

    if (where == "tab") {
      urlSecurityCheck(linkUrl, linkNode.ownerDocument.nodePrincipal);

      let bgLoad = this.loadInBackgroundPref;
      if (event.shiftKey) {
        bgLoad = !bgLoad;
      }

      this.openTab(linkUrl, bgLoad, where);
    } else {
      let copyUrlContextMenu = new nsContextMenu(event.target, event.shiftKey);
      copyUrlContextMenu.linkURL = linkUrl;
      copyUrlContextMenu.linkURI = copyUrlContextMenu.getLinkURI();
      copyUrlContextMenu.openLinkInBrowser();
    }
  },

  /**
   * Open a link in Tb, via contextmenu.
   *
   * @param {Event} event   - An event.
   * @param {String} where  - 'tab' ('window' is unused).
   */
  openLinkInContentTab(event, where) {
    let linkNode = event.target;
    let linkUrl = gContextMenu.linkURI.spec;
    // console.debug("openLinkInContentTab: START url - " + linkUrl);
    if (!linkUrl || event.detail == 2 || event.button == 2) {
      return;
    }

    urlSecurityCheck(linkUrl, linkNode.ownerDocument.nodePrincipal);

    let bgLoad = this.loadInBackgroundPref;
    if (event.shiftKey) {
      bgLoad = !bgLoad;
    }

    where = where == "window" ? "window" : "tab";
    bgLoad = where == "window" ? false : bgLoad;
    this.openTab(linkUrl, bgLoad, where);
  },

  openLinkExternally(url, event) {
    event = event || this.savedContentAreaClickEvent;
    let linkNode = event && event.target;
    let loadInTab = this.linkClickLoadsInTabPref;

    urlSecurityCheck(
      url,
      linkNode
        ? linkNode.ownerDocument.nodePrincipal
        : document.documentElement.ownerDocument.nodePrincipal
    );

    if (event && event.ctrlKey) {
      loadInTab = !loadInTab;
    }
    if (!loadInTab) {
      this._openLinkExternally(url);
      this.savedContentAreaClickEvent = null;
      return;
    }
    // console.debug("openLinkExternally: url - " + url);
    let bgLoad = this.linkClickLoadsInBackgroundTabPref;
    if (event && event.shiftKey) {
      bgLoad = !bgLoad;
    }
    this.openTab(url, bgLoad, "tab");
  },

  openTab(url, bgLoad, where) {
    // Use the ua compat pref if desired, but only for links, and set it back
    // to whatever is was before.
    let saveCompatModePref = Services.prefs.getBoolPref(
      "general.useragent.compatMode.firefox"
    );
    if (this.useFirefoxCompatUserAgentPref) {
      Services.prefs.setBoolPref("general.useragent.compatMode.firefox", true);
    }
    openTab(
      "contentTab",
      {
        contentPage: url,
        duplicate: true,
        clickHandler: "BrowseInTab.siteClickHandler(event)",
        background: bgLoad,
      },
      where
    );

    Services.prefs.setBoolPref(
      "general.useragent.compatMode.firefox",
      saveCompatModePref
    );
    this.savedContentAreaClickEvent = null;
  },

  /**
   * This handles web content tabs where the linkNode belongs to an html page.
   * System content tabs such as Preferences and Add-ons Manager where
   * the linkNode document has an |about:| scheme are not supported currently.
   */

  siteClickHandler(event) {
    let linkNode = event.target;

    if (linkNode.ownerDocument.URL.startsWith("about:")) {
      // console.debug("siteClickHandler: about: linkNode");
      return false;
    }

    if (linkNode instanceof XULElement) {
      // console.debug("siteClickHandler: xul linkNode.value - " + linkNode.value);
      return false;
    }

    let url = linkNode.href;
    // console.debug("siteClickHandler: linkNode.href - " + url);

    if (
      !url ||
      !url.startsWith("http") ||
      event.detail == 2 ||
      event.button == 2
    ) {
      return true;
    }

    // console.dir(linkNode.attributes);

    urlSecurityCheck(url, linkNode.ownerDocument.nodePrincipal);

    let forceInCurrentTab = this.forceLinkClickLoadsInCurrentTabPref;
    if (event.ctrlKey || this.savedCtrlKey) {
      forceInCurrentTab = !forceInCurrentTab;
      this.savedCtrlKey = false;
    }

    // console.debug("siteClickHandler: forceInCurrentTab - " + forceInCurrentTab);
    if (forceInCurrentTab) {
      if (linkNode.getAttribute("target") == "_blank") {
        linkNode.setAttribute("_target", linkNode.getAttribute("target"));
        linkNode.removeAttribute("target");
      }
      if (event.ctrlKey) {
        this.savedCtrlKey = true;
        linkNode.click();
      }
    } else {
      event.preventDefault();
      this.openTab(url, event && event.shiftKey, "tab");
    }
    // console.dir(linkNode.attributes);
    return false;
  },
};

// console.debug("BrowseInTab: readystate - " + window.document.readyState);

if (window.document.readyState == "complete") {
  BrowseInTab.onLoad();
} else {
  window.addEventListener(
    "load",
    () => {
      BrowseInTab.onLoad();
    },
    { once: true }
  );
}
