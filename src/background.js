"use strict";

/* globals browser */

console.info(browser.i18n.getMessage("extensionName"));

var init = async () => {
  // First, init notifyLocaleMessages and notifyStorageLocal, serially.
  browser.browseintab.notifyLocaleMessages();
  await browser.storage.local.get().then(storageLocalData => {
    browser.browseintab.notifyStorageLocal(storageLocalData, true);
  });

  // Finally, inject the main script.
  browser.browseintab.injectScriptIntoChromeDocument(
    "content/browseintab.js",
    "mail:3pane"
  );
};

init();
